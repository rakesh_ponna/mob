package com.rakesh.mobiquityweatherapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.rakesh.mobiquityweatherapp.data.db.entity.BookMarkLocation

@Database(
    entities = [BookMarkLocation::class],
    version = 1
)
abstract class MobiquityDatabase : RoomDatabase() {

    abstract fun bookMarkLocationDao(): BookMarkLocationDao

    companion object {
        @Volatile private var instance: MobiquityDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                MobiquityDatabase::class.java, "mobiquityWeatherEntries.db")
                .build()
    }
}
