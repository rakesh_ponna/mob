package com.rakesh.mobiquityweatherapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.rakesh.mobiquityweatherapp.data.db.entity.BookMarkLocation

@Dao
interface BookMarkLocationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocation(bookMarkLocation: BookMarkLocation)

    @Query("select * from book_mark_location ")
    fun getLocation(): List<BookMarkLocation>

    @Query("select * from book_mark_location ")
    fun getLocationData(): LiveData<List<BookMarkLocation>>

    @Query("select * from book_mark_location where latitude = :lat and latitude = :lng")
    fun getCurrentLocationData(lat: String, lng: String): LiveData<BookMarkLocation>

}