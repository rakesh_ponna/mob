package com.rakesh.mobiquityweatherapp.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

const val location_ID = 0

@Entity(tableName = "book_mark_location")
data class BookMarkLocation(
    val latitude: Double, val longitude: Double, val subAdminArea: String? ,val locality: String? ,val postalCode: String? ,val countryCode: String? ,val countryName: String? ,val address: String?
): Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}