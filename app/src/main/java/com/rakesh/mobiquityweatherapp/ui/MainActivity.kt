package com.rakesh.mobiquityweatherapp.ui

import android.R.menu
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.preference.PreferenceFragmentCompat
import com.rakesh.mobiquityweatherapp.R
import com.rakesh.mobiquityweatherapp.databinding.ActivityMainBinding
import com.rakesh.mobiquityweatherapp.ui.base.BaseActivity
import com.rakesh.mobiquityweatherapp.ui.fragments.HomeFragment
import com.rakesh.mobiquityweatherapp.viewmodel.MobViewModel
import com.rakesh.mobiquityweatherapp.viewmodel.ViewModelFactory
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance


class MainActivity : BaseActivity(), KodeinAware, HomeFragment.Listener {
    override val kodein by closestKodein()
    private val viewModelFactory: ViewModelFactory by instance()
    private lateinit var viewModel: MobViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MobViewModel::class.java)
        addFragment(HomeFragment.newInstance(), true, R.id.container)
    }

    override fun addFragment(fragment: Fragment, addToBackStack: Boolean) {
        addFragment(fragment, addToBackStack, R.id.container)
    }

    override fun replaceFragment(fragment: Fragment, addToBackStack: Boolean) {
        replaceFragment(fragment, addToBackStack, R.id.container)
    }

    override fun prefFragment(fragment: Fragment, addToBackStack: Boolean) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, fragment)
            .commit()    }

    private val currentFragment: Fragment?
        get() = supportFragmentManager.findFragmentById(R.id.container)


    override fun onBackPressed() {
        val mapFragment = currentFragment
        if (mapFragment?.tag == "HomeFragment")
            finishAffinity()
        else if (mapFragment?.tag == "MapFragment")
            title = "Select Location"
        else if (mapFragment?.tag == "UserSelectedFragment")
            title = "User screen"
        super.onBackPressed()
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            addPreferencesFromResource(R.xml.preferences)
        }
    }

}