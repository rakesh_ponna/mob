package com.rakesh.mobiquityweatherapp.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.rakesh.mobiquityweatherapp.R
import com.rakesh.mobiquityweatherapp.data.db.entity.BookMarkLocation
import com.rakesh.mobiquityweatherapp.databinding.HomescreenFragmentBinding
import com.rakesh.mobiquityweatherapp.ui.LocationAdapter
import com.rakesh.mobiquityweatherapp.ui.MainActivity
import com.rakesh.mobiquityweatherapp.ui.base.BaseFragment
import com.rakesh.mobiquityweatherapp.viewmodel.MobViewModel
import com.rakesh.mobiquityweatherapp.viewmodel.ViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance

class HomeFragment : BaseFragment(), KodeinAware, View.OnClickListener ,ItemClickListener{

    override val kodein by closestKodein()
    private val viewModelFactory: ViewModelFactory by instance()
    private lateinit var viewModel: MobViewModel
    private lateinit var binding: HomescreenFragmentBinding
    private var mListener: Listener? = null

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mListener = if (parentFragment != null) {
                parentFragment as Listener
            } else {
                context as Listener
            }
            activity?.title = "Home Screen"

        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement Listener")
        }
    }

    override fun initComponents() {
        setHasOptionsMenu(true);
        binding.addLocation.setOnClickListener(this)
        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(MobViewModel::class.java)
        bindUI()
    }

    override fun onClick(view: View?) {
        view?.let {
            if (view.id == R.id.add_location) {
                mListener?.addFragment(MapFragment(), true)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.homescreen_fragment, container, false
        )
        initComponents()
        return binding.root
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }


    interface Listener {
        fun addFragment(fragment: Fragment, addToBackStack: Boolean)
        fun replaceFragment(fragment: Fragment, addToBackStack: Boolean)
        fun prefFragment(fragment: Fragment, addToBackStack: Boolean)
    }

    private fun bindUI() = launch(Dispatchers.Main) {
        val locationEntries = viewModel._data.await()

        locationEntries.observe(activity!!, Observer { entries ->
            if (entries == null) return@Observer
            initRecyclerView(entries)
        })
    }

    private fun initRecyclerView(locations: List<BookMarkLocation>) {
        binding.recyclerView.adapter = LocationAdapter(activity!!, this,locations)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_dashboard, menu);
        menu.findItem(R.id.action_bookmark).isVisible = false;
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_setting -> {
                mListener?.prefFragment(MainActivity.SettingsFragment(), true)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onItemClick(data: BookMarkLocation) {
        mListener?.replaceFragment(UserSelectedFragment.newInstance(data.latitude,data.longitude), true)
    }

}

interface ItemClickListener {
    fun onItemClick(data: BookMarkLocation)
}