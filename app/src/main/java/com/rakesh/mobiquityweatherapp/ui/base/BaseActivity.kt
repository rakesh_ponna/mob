package com.rakesh.mobiquityweatherapp.ui.base

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.rakesh.mobiquityweatherapp.R

open class BaseActivity : AppCompatActivity() {

    //endregion
    // region Fragment transactions
    fun replaceFragment(
        fragment: Fragment,
        addToBackStack: Boolean,
        frameLayoutId: Int
    ) {
        val tag = fragment.javaClass.simpleName
        val fragmentTransaction =
            supportFragmentManager.beginTransaction()
        setCustomAnimation(fragmentTransaction, false)
        if (addToBackStack) fragmentTransaction.replace(frameLayoutId, fragment, tag)
            .addToBackStack(tag).commit() else fragmentTransaction.replace(
            frameLayoutId,
            fragment,
            tag
        ).commit()
    }

    fun addFragment(
        fragment: Fragment,
        addToBackStack: Boolean,
        frameLayoutId: Int
    ) {
        val tag = fragment.javaClass.simpleName
        val fragmentTransaction =
            supportFragmentManager.beginTransaction()
        setCustomAnimation(fragmentTransaction, false)
        if (addToBackStack) fragmentTransaction.add(frameLayoutId, fragment, tag)
            .addToBackStack(tag).commit() else fragmentTransaction.add(frameLayoutId, fragment, tag)
            .commit()
    }


    private fun setCustomAnimation(
        ft: FragmentTransaction,
        reverseAnimation: Boolean
    ) {
        if (!reverseAnimation) {
            ft.setCustomAnimations(
                R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right
            )
        } else {
            ft.setCustomAnimations(
                R.anim.enter_from_left, R.anim.exit_to_right,
                R.anim.enter_from_right, R.anim.exit_to_left
            )
        }
    }
    //endregion
}