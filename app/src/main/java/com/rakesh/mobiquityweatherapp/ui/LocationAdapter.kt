package com.rakesh.mobiquityweatherapp.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.rakesh.mobiquityweatherapp.BR
import com.rakesh.mobiquityweatherapp.R
import com.rakesh.mobiquityweatherapp.data.db.entity.BookMarkLocation
import com.rakesh.mobiquityweatherapp.databinding.ItemLocationsBinding
import com.rakesh.mobiquityweatherapp.ui.fragments.ItemClickListener

class LocationAdapter(var context: Context, var itemListner : ItemClickListener,
                      var list: List<BookMarkLocation>) : RecyclerView.Adapter<LocationAdapter.ViewHolder>() {

    // Inflating Layout and ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemLocationsBinding>(layoutInflater, R.layout.item_locations, parent, false)
        return ViewHolder(binding).listen{ pos, _ ->
            val item = list[pos]
            itemListner.onItemClick(item)

        }
    }

    override fun getItemCount(): Int = list.size

    // Bind data
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    // Creating ViewHolder
    class ViewHolder(private val binding: ItemLocationsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Any) {
            binding.setVariable(BR.location, data)
            binding.executePendingBindings()
        }
    }
    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(adapterPosition, itemViewType)
        }
        return this
    }
}