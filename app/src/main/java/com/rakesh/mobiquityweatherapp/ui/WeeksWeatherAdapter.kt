package com.rakesh.mobiquityweatherapp.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rakesh.mobiquityweatherapp.BR
import com.rakesh.mobiquityweatherapp.R
import com.rakesh.mobiquityweatherapp.databinding.ItemDaysWeatherBinding
import com.rakesh.mobiquityweatherapp.network.response.Daily


class WeeksWeatherAdapter(
    var context: Context,
    var list: List<Daily>
) : RecyclerView.Adapter<WeeksWeatherAdapter.ViewHolder>() {

    // Inflating Layout and ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemDaysWeatherBinding>(
            layoutInflater,
            R.layout.item_days_weather,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = list.size

    // Bind data
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
        holder.bindimage(list[position].weather[0].icon)
    }

    // Creating ViewHolder
    class ViewHolder(private val binding: ItemDaysWeatherBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(value: Any) {
            binding.setVariable(BR.daily, value)
             binding.executePendingBindings()
        }
        fun bindimage(value: Any) {
            var iconurl = "http://openweathermap.org/img/w/$value.png";
            Glide.with(binding.weatherImageView)
                .load(iconurl).apply(RequestOptions().centerCrop())
                .into(binding.weatherImageView)
//            binding.setVariable(BR.imageUrl, iconurl)
//            binding.weatherImageView.setImageURI()
        }
    }
//    companion object {
//        @BindingAdapter("weatherimage")
//        @JvmStatic
//        fun loadImage(view: ImageView, imageUrl: String?) {
//            Glide.with(view.context)
//                .load(imageUrl).apply(RequestOptions().centerCrop())
//                .into(view)
//        }
//    }
}
