package com.rakesh.mobiquityweatherapp.ui.fragments

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.rakesh.mobiquityweatherapp.R
import com.rakesh.mobiquityweatherapp.data.db.BookMarkLocationDao
import com.rakesh.mobiquityweatherapp.data.db.entity.BookMarkLocation
import com.rakesh.mobiquityweatherapp.databinding.FragmentMapBinding
import com.rakesh.mobiquityweatherapp.ui.base.BaseFragment
import com.rakesh.mobiquityweatherapp.viewmodel.MobViewModel
import com.rakesh.mobiquityweatherapp.viewmodel.ViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance
import java.util.*


@Suppress("DEPRECATION")
class MapFragment : BaseFragment(), KodeinAware {
    override val kodein by closestKodein()
    private val bookMarkLocationDao: BookMarkLocationDao by instance()
    private val viewModelFactory: ViewModelFactory by instance()
    private lateinit var viewModel: MobViewModel
    private lateinit var binding: FragmentMapBinding
    private lateinit var rootView: View

    private var latLong  = LatLng(17.3850, 78.4867)
    var googleMap: GoogleMap? = null
    private val TAG = "MAP LOCATION"
    internal var mLocationRequest: LocationRequest? = null
    private val UPDATE_INTERVAL = (10 * 1000).toLong()  /* 10 secs */
    private val FASTEST_INTERVAL: Long = 2000 /* 2 sec */
    private var geocoder: Geocoder? = null
    private var address: Address? = null

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()

        protected fun startLocationUpdates(mapFragment: MapFragment) {
            // initialize location request object
            mapFragment.mLocationRequest = LocationRequest.create()
            mapFragment.mLocationRequest!!.run {
                setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                setInterval(mapFragment.UPDATE_INTERVAL)
                setFastestInterval(mapFragment.FASTEST_INTERVAL)
            }

            // initialize location setting request builder object
            val builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(mapFragment.mLocationRequest!!)
            val locationSettingsRequest = builder.build()

            // initialize location service object
            val settingsClient =
                LocationServices.getSettingsClient(mapFragment.activity!!.applicationContext)
            settingsClient.checkLocationSettings(locationSettingsRequest)

            // call register location listener
            mapFragment.registerLocationListner()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MobViewModel::class.java)
        setHasOptionsMenu(true);
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
         inflater.inflate(R.menu.menu_boomark, menu);
        menu.findItem(R.id.action_setting).isVisible = false;
        super.onCreateOptionsMenu(menu, inflater)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.action_bookmark -> {
                insertBookMarkLocations(bookmarkDataReturn())
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    protected fun insertBookMarkLocations(bmk: BookMarkLocation) {
        GlobalScope.launch(Dispatchers.IO) {
            bookMarkLocationDao.insertLocation(bmk)
        }
        activity!!.onBackPressed()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false)
        rootView = binding.root
        initComponents()
        return rootView
    }

    @SuppressLint("MissingPermission")
    override fun initComponents() {
        //for crate home button
        geocoder = Geocoder(context, Locale.getDefault())
        val supportMapFragment: SupportMapFragment =
            childFragmentManager.findFragmentById(R.id.mapfragment) as SupportMapFragment
        supportMapFragment.getMapAsync {
            googleMap = it
            googleMap?.addMarker(MarkerOptions().position(latLong))
            googleMap?.setOnCameraChangeListener { cameraPosition ->
                latLong = cameraPosition.target
                googleMap?.clear()
                val fromLocation = geocoder?.getFromLocation(latLong.latitude, latLong.longitude, 1)
                if(fromLocation?.size!! > 0){
                    address = fromLocation?.get(0)
                    binding.locationMarkertext.text = address?.getAddressLine(0)
                }else{
                    binding.locationMarkertext.text = "${latLong.latitude}, ${latLong.longitude}"
                }
            }
            val mLocation = Location("")
            mLocation.latitude = latLong.latitude
            mLocation.longitude = latLong.longitude
            mLocation.changeMap()
        }
    }

    private fun Location.changeMap() {
        if (googleMap != null) {
            googleMap!!.uiSettings.isZoomControlsEnabled = false
            latLong = LatLng(latitude, longitude)
            val cameraPosition: CameraPosition = CameraPosition.Builder()
                .target(latLong).zoom(19f).tilt(70f).build()
            googleMap!!.uiSettings.isMyLocationButtonEnabled = true
            googleMap!!.animateCamera(
                CameraUpdateFactory
                    .newCameraPosition(cameraPosition)
            )
            val fromLocation = geocoder?.getFromLocation(latLong.latitude, latLong.longitude, 1)
            if(fromLocation?.size!! > 0){
                address = fromLocation[0]
                binding.locationMarkertext.text = address?.getAddressLine(0)
            }else{
                binding.locationMarkertext.text = "${latLong.latitude}, ${latLong.longitude}"
            }
        } else {
            Toast.makeText(
                activity!!.applicationContext,
                "Sorry! unable to create maps", Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun bookmarkDataReturn() = BookMarkLocation(
        address!!.latitude,
        address!!.longitude,
        address!!.subAdminArea,
        address!!.locality,
        address!!.postalCode,
        address!!.countryCode,
        address!!.countryName,
        binding.locationMarkertext.text.toString()
    )


    internal fun checkPermission(): Boolean = if (ContextCompat.checkSelfPermission(
            activity!!.applicationContext,
            android.Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    ) {
        true;
    } else {
        requestPermissions()
        false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            activity!!,
            arrayOf("Manifest.permission.ACCESS_FINE_LOCATION"),
            1
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (permissions[0] == android.Manifest.permission.ACCESS_FINE_LOCATION) {
                registerLocationListner()
            }
        }
    }


}

private fun MapFragment.registerLocationListner() {
    // initialize location callback object
    val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
        }
    }
    // 4. add permission if android version is greater then 23
    if (checkPermission()) {
        LocationServices.getFusedLocationProviderClient(activity!!.applicationContext)
            .requestLocationUpdates(mLocationRequest, locationCallback, Looper.myLooper())
    }
}