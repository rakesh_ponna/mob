package com.rakesh.mobiquityweatherapp.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.rakesh.mobiquity.network.response.WeatherDataModel
import com.rakesh.mobiquityweatherapp.R
import com.rakesh.mobiquityweatherapp.databinding.FragemntUserSelectedCityBinding
import com.rakesh.mobiquityweatherapp.network.ApiService
import com.rakesh.mobiquityweatherapp.network.repository.ApiHelper
import com.rakesh.mobiquityweatherapp.network.response.Current
import com.rakesh.mobiquityweatherapp.ui.WeeksWeatherAdapter
import com.rakesh.mobiquityweatherapp.ui.base.BaseFragment
import com.rakesh.mobiquityweatherapp.util.Status
import com.rakesh.mobiquityweatherapp.viewmodel.UserSelectionViewModel
import com.rakesh.mobiquityweatherapp.viewmodel.ViewModelFactoryModel
import java.lang.String
import java.util.*

class UserSelectedFragment : BaseFragment() {
    private lateinit var viewModel: UserSelectionViewModel
    private var latitude: Double = 17.3833
    private var longitude: Double = 78.4861
    private lateinit var binding: FragemntUserSelectedCityBinding

    companion object {
        @JvmStatic
        fun newInstance(latitude: Double, longitude: Double) = UserSelectedFragment().apply {
            arguments = Bundle().apply {
                putDouble("latitude", latitude)
                putDouble("longitude", longitude)
            }
        }
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_dashboard, menu);
        menu.findItem(R.id.action_bookmark).isVisible = false;
        menu.findItem(R.id.action_setting).isVisible = false;
        super.onCreateOptionsMenu(menu, inflater)
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.title = "User Screen"
        latitude = arguments?.getDouble("latitude")!!
        longitude = arguments?.getDouble("longitude")!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragemnt_user_selected_city, container, false
        )
        initComponents()
        return binding.root
    }

    override fun initComponents() {
        binding.progressBar.visibility = View.GONE
        viewModel = ViewModelProvider(
            this,
            ViewModelFactoryModel(ApiHelper(ApiService.RetrofitBuilder.apiService))
        ).get(UserSelectionViewModel::class.java)
        setupObserverCall()
    }

    private fun setupObserverCall() {
        viewModel.getWeatherDetails(latitude, longitude).observe(activity!!, Observer {
            it?.let { resource ->
                println(resource.data)
                when (resource.status) {
                    Status.SUCCESS -> {
                        binding.progressBar.visibility = View.GONE
                        updateUI(resource.data)
                    }
                    Status.ERROR -> {
                        binding.progressBar.visibility = View.GONE
                    }
                    Status.LOADING -> {
                        binding.progressBar.visibility = View.VISIBLE
                    }

                }
            }
        })
    }

    private fun updateUI(data: WeatherDataModel?) {
        var current: Current? = data?.current
        binding.descriptionTextView.text = current?.weather!![0].description
        binding.windTextView.text = String.format(
            Locale.getDefault(),
            resources.getString(R.string.wind_unit_label),
            current.wind_speed
        )
        binding.tempTextView.text = String.format(
            Locale.getDefault(),
            "%.0f°",
            current.temp
        )
        binding.humidityTextView.text = String.format(
            Locale.getDefault(),
            "%d%%",
            current.humidity
        )
        if (data != null) {
            binding.recyclerView.adapter = WeeksWeatherAdapter(
                activity!!,
                data.daily
            )
        }
    }

}