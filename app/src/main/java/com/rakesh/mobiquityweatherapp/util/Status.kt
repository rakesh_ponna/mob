package com.rakesh.mobiquityweatherapp.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}