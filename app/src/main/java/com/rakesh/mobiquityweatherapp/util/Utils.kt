package com.rakesh.mobiquityweatherapp.util

import android.content.Context
import androidx.preference.PreferenceManager

const val UNIT_SYSTEM = "UNIT_SYSTEM"

fun getUnitSystem(context: Context): UnitSystem {
    val selectedName = PreferenceManager.getDefaultSharedPreferences(context).getString(UNIT_SYSTEM, UnitSystem.METRIC.name)
    return UnitSystem.valueOf(selectedName!!)
}

enum class UnitSystem {
    METRIC, IMPERIAL
}