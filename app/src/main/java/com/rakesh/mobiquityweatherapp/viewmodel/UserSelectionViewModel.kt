package com.rakesh.mobiquityweatherapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.rakesh.mobiquityweatherapp.network.repository.MainRepository
import com.rakesh.mobiquityweatherapp.network.repository.Resource
import kotlinx.coroutines.Dispatchers

class UserSelectionViewModel(private val mainRepository: MainRepository) : ViewModel() {

    fun getWeatherDetails(lat: Double, lng: Double) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.getWeather(lat, lng)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}