package com.rakesh.mobiquityweatherapp.viewmodel

import androidx.lifecycle.ViewModel
import com.rakesh.mobiquityweatherapp.data.db.BookMarkLocationDao
import com.rakesh.mobiquityweatherapp.internal.lazyDeferred
import com.rakesh.mobiquityweatherapp.network.ApiService

abstract class SharedViewModelData(
    private val dao: BookMarkLocationDao
) : ViewModel() {
    val location by lazyDeferred {
        dao.getLocationData()
    }
}