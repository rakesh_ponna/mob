package com.rakesh.mobiquityweatherapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rakesh.mobiquityweatherapp.data.db.BookMarkLocationDao
import com.rakesh.mobiquityweatherapp.network.ApiService

class ViewModelFactory(
    private val dao: BookMarkLocationDao
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MobViewModel(
            dao
        ) as T
    }
}