package com.rakesh.mobiquityweatherapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rakesh.mobiquityweatherapp.network.repository.ApiHelper
import com.rakesh.mobiquityweatherapp.network.repository.MainRepository

class ViewModelFactoryModel(private val apiHelper: ApiHelper) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserSelectionViewModel::class.java)) {
            return UserSelectionViewModel(MainRepository(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}