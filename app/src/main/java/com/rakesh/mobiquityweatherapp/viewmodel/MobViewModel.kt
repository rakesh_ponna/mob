package com.rakesh.mobiquityweatherapp.viewmodel

import com.rakesh.mobiquityweatherapp.data.db.BookMarkLocationDao
import com.rakesh.mobiquityweatherapp.internal.lazyDeferred
import com.rakesh.mobiquityweatherapp.network.ApiService

class MobViewModel(
    private val dao: BookMarkLocationDao
) : SharedViewModelData(dao) {

     val _data by lazyDeferred {
        dao.getLocationData()
    }

}