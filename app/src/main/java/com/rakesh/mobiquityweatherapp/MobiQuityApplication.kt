package com.rakesh.mobiquityweatherapp

import android.app.Application
import android.preference.PreferenceManager
import com.rakesh.mobiquityweatherapp.data.db.MobiquityDatabase
import com.rakesh.mobiquityweatherapp.viewmodel.ViewModelFactory
import com.resocoder.forecastmvvm.data.network.ConnectivityInterceptor
import com.rakesh.mobiquityweatherapp.network.ConnectivityInterceptorImpl
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MobiQuityApplication : Application(),KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@MobiQuityApplication))
        bind() from singleton { MobiquityDatabase(instance()) }
        bind() from singleton { instance<MobiquityDatabase>().bookMarkLocationDao() }
        bind<ConnectivityInterceptor>() with singleton { ConnectivityInterceptorImpl(instance()) }
        bind() from provider { ViewModelFactory(instance()) }

    }

    override fun onCreate() {
        super.onCreate()
    }
}