package com.rakesh.mobiquityweatherapp.network.response

data class Rain(
    val `1h`: Double
)