package com.rakesh.mobiquityweatherapp.network.response

data class Hourly(
    val clouds: Int,
    val dew_point: Double,
    val dt: Int,
    val feels_like: Double,
    val humidity: Int,
    val pop: Int,
    val pressure: Int,
    val rain: Rain,
    val temp: Double,
    val uvi: String,
    val visibility: Int,
    val weather: List<Weather>,
    val wind_deg: String,
    val wind_speed: Double
)