package com.rakesh.mobiquityweatherapp.network.repository

class MainRepository(private val apiHelper: ApiHelper) {

    suspend fun getWeather(lat:Double ,lng:Double) = apiHelper.getWeather(lat,lng)
}