package com.rakesh.mobiquity.network.response

import com.rakesh.mobiquityweatherapp.network.response.Current
import com.rakesh.mobiquityweatherapp.network.response.Daily
import com.rakesh.mobiquityweatherapp.network.response.Hourly
import com.rakesh.mobiquityweatherapp.network.response.Minutely

data class WeatherDataModel(
    val current: Current,
    val daily: List<Daily>,
    val hourly: List<Hourly>,
    val lat: Double,
    val lon: Double,
    val minutely: List<Minutely>,
    val timezone: String,
    val timezone_offset: Int
)