package com.rakesh.mobiquityweatherapp.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.rakesh.mobiquity.network.response.WeatherDataModel
 import com.resocoder.forecastmvvm.data.network.ConnectivityInterceptor
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*


const val API_KEY = "48681f13bbeb01df675182bc52263419"
const val BASE_URL = "http://api.openweathermap.org/data/2.5/"


//https://api.openweathermap.org/data/2.5/onecall?lat=17.3895&lon=-78.3576&appid=48681f13bbeb01df675182bc52263419
interface ApiService {

    @GET("onecall")
    suspend fun getWeatherByLatlng(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("appid") apikey: String = API_KEY
    ): WeatherDataModel


    object RetrofitBuilder {
        private fun getRetrofit(): Retrofit {
            val httpClient = OkHttpClient.Builder()
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging)
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl(BASE_URL)
                .build() //Doesn't require the adapter
        }


        val apiService: ApiService = getRetrofit().create(ApiService::class.java)
    }
}