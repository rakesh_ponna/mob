package com.rakesh.mobiquityweatherapp.network.response

data class Minutely(
    val dt: Int,
    val precipitation: Int
)