package com.rakesh.mobiquityweatherapp.network.repository

import com.rakesh.mobiquityweatherapp.network.API_KEY
import com.rakesh.mobiquityweatherapp.network.ApiService

class ApiHelper(private val apiService: ApiService) {

    suspend fun getWeather(lat:Double ,lng:Double) = apiService.getWeatherByLatlng(lat,lng, API_KEY)
}