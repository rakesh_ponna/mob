package com.rakesh.mobiquityweatherapp.internal

import java.io.IOException


class NoConnectivityException: IOException()
